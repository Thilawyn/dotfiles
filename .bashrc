#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Pywal
(cat ~/.cache/wal/sequences &)

# Powerline
source /usr/share/powerline/bindings/bash/powerline.sh

# Aliases
#alias ls="ls --color=auto"
alias ls="lsd"
alias lsla="ls -la"
alias lsfonts="fc-list | cut -f2 -d: | sort -u"

alias find-pacfiles="find /etc -regextype posix-extended -regex '.+\.pac(new|save)' 2>/dev/null"

alias ssh="TERM=xterm-256color ssh"

alias cf="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

WEBSERVICES="openresty php-fpm mariadb"
alias start-webservices="sudo sh -c 'echo \"Démarrage des services web...\"; systemctl start $WEBSERVICES'"
alias restart-webservices="sudo sh -c 'echo \"Redémarrage des services web...\"; systemctl restart $WEBSERVICES'"
alias stop-webservices="sudo sh -c 'echo \"Arrêt des services web...\"; systemctl stop $WEBSERVICES'"

alias drop-caches="sudo sh -c 'sync; echo 3 > /proc/sys/vm/drop_caches'"

alias deemix-debian="TERM=xterm-256color sudo systemd-nspawn -D ~/chroots/deemix-debian/ -u $USER --chdir=/home/$USER bash --login"

# Variables
PS1="[\u@\h \W]\$ "
