#!/usr/bin/env sh

#
# ~/.profile
#

# Device type
case $(hostname) in
	thilaptop) export DEVICE_TYPE=laptop

	           export IS_LAPTOP=true
	           export IS_DESKTOP=false;;

	*)         export DEVICE_TYPE=desktop

	           export IS_LAPTOP=false
	           export IS_DESKTOP=true
esac

# Path
export PATH=$(du ~/.local/bin | cut -f2 | tr '\n' ':' | sed 's/:*$//'):~/.config/composer/vendor/bin:$PATH

# Chroots
export AUR_CHROOT=~/chroots/aur-chroot
export AUR_CHROOT_USER=builder

# Other environment variables
export MPD_HOST=~/.mpd/socket

# SSH agent
eval $(ssh-agent)

# Start X if on TTY1
[ ! $DISPLAY ] && [ $XDG_VTNR -eq 1 ] && exec startx -- vt1 >/dev/null 2>&1
